import { getNotes } from './modules/api-client.js';
import { displayNewNoteModal } from './modules/note.js';
import { displayNotesList } from './modules/notes-list.js';

/**
 * Function which runs only once, after page is fully loaded
 */
document.addEventListener("DOMContentLoaded", () => {
    getNotes().then(displayNotesList);
    document.getElementById("addNoteButton").addEventListener("click", displayNewNoteModal);
});


