import * as apiClient from './api-client.js';
import { addNote, displayNotesList } from './notes-list.js';

/**
 * Gets HTML note object and adds to noteModal
 * 
 * @param {Object} note - note object
 */
export const displayNoteModal = (note) => {
    const noteModalTemplate =
        `<div class="modal-header mb-0 pb-0">
            <h5 class="modal-title">${note.title}</h5>
            <div class="line"></div>
            <div class="btn-group float-right mb-2" role="group">
                <button id="editNoteButton" type="button" class="btn float-right modal-button-top" data-toggle="tooltip" data-placement="top" title="Edytuj">
                    <span aria-hidden="true">
                        <i class="far fa-edit"></i>
                    </span>
                </button>
                <button id="deleteNoteButton" type="button" class="btn float-right modal-button-top" data-toggle="tooltip" data-placement="top" title="Usuń notatkę">
                    <span aria-hidden="true">
                        <i class="far fa-trash-alt"></i>
                    </span>
                </button>
                <button type="button" class="btn float-right modal-button-top" data-dismiss="modal" aria-label="Close" data-toggle="tooltip"
                    data-placement="top" title="Zamknij">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        <div class="modal-body">${note.content}</div>
    `;

    let noteModalContent = document.getElementById("noteModalContent"); 
    noteModalContent.innerHTML = ''; // crearing noteModal after use, avoids creating two modals with notes side by side
    noteModalContent.insertAdjacentHTML('beforeend', noteModalTemplate); //working innerHTML

    document.getElementById("deleteNoteButton").addEventListener("click", () => {
        let deleteConfirmationButton = document.getElementById('choiceDeletingNoteModalYesButton');
        deleteConfirmationButton.addEventListener('click', () => {
            apiClient.deleteNote(note.id).then(() => {
                apiClient.getNotes().then(displayNotesList);
                $('#choiceDeletingNoteModal').modal('hide');
            });
        });
        $('#noteModal').modal('hide');
        $('#choiceDeletingNoteModal').modal('show');
    });
    document.getElementById("editNoteButton").addEventListener("click", () => {
        displayEditNoteModal(note);
    });
    $('#noteModal').modal('show');
}

export const displayNewNoteModal = () => {
    const newNoteTemplate = `
    <form id="newNoteForm" autocomplete="off">
        <div class="modal-header mb-0 pb-0">
            <div class="form-group">
                <input name="title" type="text" class="form-control form-note-text" placeholder="Tytuł notatki">
            </div>
            <div class="btn-group-toggle float-right" data-toggle="buttons">
                <label class="btn radio-color note-color-blue btn-circle m-0 active">
                    <input type="radio" name="color" value="blue" autocomplete="off" checked>
                </label>
                <label class="btn radio-color note-color-green btn-circle m-0">
                    <input type="radio" name="color" value="green" autocomplete="off">
                </label>
                <label class="btn radio-color note-color-orange btn-circle m-0">
                    <input type="radio" name="color" value="orange" autocomplete="off">
                </label>
                <label class="btn radio-color note-color-yellow btn-circle m-0">
                    <input type="radio" name="color" value="yellow" autocomplete="off">
                </label>
                <label class="btn radio-color note-color-lavender btn-circle m-0">
                    <input type="radio" name="color" value="lavender" autocomplete="off">
                </label>
            </div>
            <button type="button" class="btn float-right modal-button-top" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <textarea name="content" class="form-control form-note-text" placeholder="Treść notatki" rows="8"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn modal-button" value="Dodaj notatkę">
        </div>
    </form>`;

    let noteModalContent = document.getElementById("noteModalContent");
    noteModalContent.innerHTML = ''; // crearing noteModal after use, avoids creating two modals with notes side by side
    noteModalContent.insertAdjacentHTML('beforeend', newNoteTemplate); //working innerHTML
    
    document.getElementById("newNoteForm").addEventListener("submit", (event) => {
        event.preventDefault(); // Cancels sumbit event on form 
        const form = new FormData(event.target);
        apiClient.postNote(form.get('title'), form.get('content'), form.get('color')).then(addNote);
        $("#noteModal").modal("hide");
    });

    $('#noteModal').modal('show');
}

export const displayEditNoteModal = (note) => {
    const editNoteTemplate = `
    <form id="editNoteForm" autocomplete="off">
        <div class="modal-header mb-0 pb-0">
            <div class="form-group">
                <input name="title" type="text" class="form-control form-note-text" value="${note.title}">
            </div>
            <div class="btn-group-toggle float-right" data-toggle="buttons">
                <label class="btn radio-color note-color-blue btn-circle m-0 ${note.color === "blue" ? "active" : ""}">
                    <input type="radio" name="color" value="blue" autocomplete="off" checked>
                </label>
                <label class="btn radio-color note-color-green btn-circle m-0 ${note.color === "green" ? "active" : ""}">
                    <input type="radio" name="color" value="green" autocomplete="off">
                </label>
                <label class="btn radio-color note-color-orange btn-circle m-0 ${note.color === "orange" ? "active" : ""}">
                    <input type="radio" name="color" value="orange" autocomplete="off">
                </label>
                <label class="btn radio-color note-color-yellow btn-circle m-0 ${note.color === "yellow" ? "active" : ""}">
                    <input type="radio" name="color" value="yellow" autocomplete="off">
                </label>
                <label class="btn radio-color note-color-lavender btn-circle m-0 ${note.color === "lavender" ? "active" : ""}">
                    <input type="radio" name="color" value="lavender" autocomplete="off">
                </label>
            </div>
            <button type="button" class="btn float-right modal-button-top" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <textarea name="content" class="form-control form-note-text" placeholder="Treść notatki" rows="8">${note.content}</textarea>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" class="btn modal-button" value="Zapisz zmiany">
        </div>
    </form>`;

    let noteModalContent = document.getElementById("noteModalContent");
    noteModalContent.innerHTML = ''; // crearing noteModal after use, avoids creating two modals with notes side by side
    noteModalContent.insertAdjacentHTML('beforeend', editNoteTemplate); //working innerHTML

    document.getElementById("editNoteForm").addEventListener("submit", (event) => {
        event.preventDefault(); // Cancels sumbit event on form 
        const form = new FormData(event.target);
        apiClient.patchNote(note.id, form.get('title'), form.get('content'), form.get('color')).then((note) => {
            apiClient.getNotes().then(displayNotesList);
            displayNoteModal(note);
        });
    });

    $('#noteModal').modal('show');
}
