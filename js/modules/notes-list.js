import { displayNoteModal } from './note.js';
import { getOneNote } from './api-client.js';

/**
 * Adds HTML template to notes list for Note object passed as parameter
 * 
 * @param {Object} note - note object
 */
export const addNote = (note) => {
    const noteTemplate =
        `<div id="note-${note.id}" class="m-3 card note note-color-${note.color}" data-id="${note.id}">
            <div class="card-body">
                <h5 class="card-title note-title">${note.title}</h5>
                <div class="line"></div>
                <p class="card-text note-text">${note.content}</p>
                <div class="triangle"></div>
                <div class="shadow-triangle"></div>
            </div>
        </div>`;

    let noteList = document.getElementById("notesList");
    noteList.insertAdjacentHTML('beforeend', noteTemplate); 

    document.getElementById("note-" + note.id).addEventListener("click", () => {
       getOneNote(note.id).then(displayNoteModal);
    });
}

export const displayNotesList = (notes) => {
    document.getElementById("notesList").innerHTML = '';
    notes.forEach((note) => {
        addNote(note);
    });
}

$('input[type=radio][name="colorFilter"]').change((event) => {
    const color = event.target.value;

    $(`.note.note-color-${color}`).show();
    $('.note').not(`.note-color-${color}`).hide();
});
$('#clearFilters').click(() => {
    $('.note').show();
});