const API_URL = 'http://frontapp.webd.pro/notes/api';
const API_NOTES_URL = `${API_URL}/notes`;

/**
 * Function sending AJAX GET request, loading single note
 * 
 * @param {id} Number
 * @return {Promise}
 */
export const getOneNote = (id) => 
    fetch(`${API_NOTES_URL}/${id}`).then((response) => response.json());

/**
 * Function sending AJAX GET request, loading notes list
 * 
 * @return {Promise}
 */
export const getNotes = () => 
    fetch(API_NOTES_URL).then((response) => response.json());

/**
 * Function sending AJAX POST request adding new note.
 * 
 * @param {String} title 
 * @param {String} content 
 * @param {String} color 
 * @return {Promise}
 */
export const postNote = (title, content, color) => {
    const data = {
        "title": title,
        "content": content,
        "color": color
    };

    return fetch(`${API_NOTES_URL}`, {
        method: "POST",
        headers: {"Content-type": "application/json"},
        body: JSON.stringify(data)
    }).then((response) => response.json());
}

/**
 * Function sending AJAX PATCH request editing note.
 * 
 * @param {Number} id
 * @param {String} title 
 * @param {String} content 
 * @param {String} color 
 * @return {Promise}
 */
export const patchNote = (id, title, content, color) => {
    const data = {
        "title": title,
        "content": content,
        "color": color
    };
    
    return fetch(`${API_NOTES_URL}/${id}`, {
        method: 'PATCH',
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(data)
    }).then((response) => response.json());
}

/**
 * Function sending AJAX DELETE request deleting note by id.
 * 
 * @param {Number} id - numeric note id
 * @return {Promise}
 */
export const deleteNote = (id) => 
    fetch(`${API_NOTES_URL}/${id}`, {
        method: "DELETE"
    });