### Notes 
Notes app allows you to create new notes with assigned color. You can also edit its contents or delete whole note as you need. In case when you have a lot of notes, functionality of filtering by color on left panel may be useful.
I learned how to use AJAX starting from using pure JavaScript and XMLHttpRequest, then rewriting the functions so that they would use Promises and then Fetch API. Working with data from server was main goal of that project. 
In future Notes app will have new features introduced, including sorting (by creation date, alphabetically etc.) option.
#### Used technologies:
* HTML5
* CSS3
* JavaScript
* jQuery
* Bootstrap4
* ES5+
Background pattern comes from -> https://www.toptal.com/designers/subtlepatterns/skulls/ edited in Adobe Photoshop.